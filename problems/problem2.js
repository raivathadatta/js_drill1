// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*"

function findLastCar(data){
    

    var car = data[0];
    if(Array.isArray(data)){

        for (var index =1 ;index <data.length;index++){
            if((car.car_year) <  data[index].car_year){
                car = data[index]
            }
        }
       return car
       
    }
}
export default findLastCar;